# Basketball plots
This article is intended show users how to create bubble charts and hexbin plots using plotly / plotly express.

I use basketball data from the 2018-2019 NBA season as the example.

## Data
- srcdata/league_hexbin_stats.pickle are pickled dictionary files, primarily as produced by
matplotlib.hexbin.
- srcdata/[TEAMCODE]_hexbin_stats.pickle are similar data for each team.
- srcdata/league_shots_by_dist.csv includes data of shots as grouped simply by distance from the rim 

### Article
Link: TBC