# Repo - Online articles

A grabbag of various scripts and data files for my online articles

## Getting Started

Each set of datafiles and scripts will be organised in their own subdirectories, with their own README file or text note (unless I've forgotten :( )).

### Prerequisites

Python (and patience for any of my mistakes or omissions) 

## Article list:

### Interactive maps with Python, Pandas and Plotly
* Directory: mapping_blogs
* Link: https://towardsdatascience.com/interactive-maps-with-python-pandas-and-plotly-following-bloggers-through-sydney-c24d6f30867e
### Basketball analytics & visualization with Python & Plotly
* Directory: basketball_lots
* Link: https://towardsdatascience.com/interactive-basketball-data-visualizations-with-plotly-8c6916aaa59e
### Interactive climate data visualizations with Python & Plotly
* Directory: climate_data
* Link: https://towardsdatascience.com/interactive-climate-data-visualizations-with-python-plotly-de0472490b09
### Identifying optimal NBA physiques through data exploration with Python & Plotly
* Directory: nba_physiques
* Link: https://towardsdatascience.com/finding-optimal-nba-physiques-using-data-visualization-with-python-6ce27ac5b68f
### Visualizing Data Relationships in the NBA with Python and Plotly
* Directory: basketball_assists
* Link: https://towardsdatascience.com/how-to-visualize-hidden-relationships-in-data-with-python-analysing-nba-assists-f6e2edc1ab5

